HIRMEOS Clients
===============

Python clients for interacting with APIs that were developed as part of the
OPERAS Metrics services - originally developed during the HIRMEOS project.


Release Notes:
==============

[0.1.9] - 2024-06-10
---------------------

Added:
 - Updated respose

Changed:
 - Added 403 to the authentication_request method.


[0.1.8] - 2023-11-16
---------------------

Added:
 - Updated tests

Changed:
 - AltmetricsClient.get_token function split between two methods, get_token
   and set_token
 - Added token_has_expired optional parameter to AltmetricsClient.set_header()


[0.1.7] - 2023-11-15
---------------------

Added:
 - Updated tests
 - added an authenticated post request method to the Altmetrics client


[0.1.6] - 2022-11-04
---------------------

Added:
 - Update tests (minor)

Changed:
 - Update PyJWT requirement, as lower versions had security vulnerabilities.


[0.1.5] - 2022-05-20
---------------------

Added:
 - Add tag uri schemes to variables list.


[0.1.4] - 2021-12-02
---------------------

Changed:
 - Update processing canonical identifiers in the translator client logic to
   allow multiple works to be returned when `uri_strict=True` is used.


[0.1.3] - 2021-10-20
---------------------

Changed:
 - Update requirements to be more flexible


[0.1.2] - 2021-10-15
---------------------

Added:
 - Allow canonical URIs to be set when creating works in the translator service
 - Main identifier will be registered as a canonical identifier when creating a
   work


[0.1.1] - 2021-10-15
---------------------

Added:
 - Add method to delete URIs from a work
 - Add method to update/set the canonical flag for a URI


[0.1.0] - 2021-02-02
---------------------

Added:
 - Unit Tests for Translator Client
 - Translator Client attributes: `remove_uri_trailing_slash` &
   `use_lower_case_uris`. These automatically format URIs for consistency.


[0.0.20] - 2021-01-26
---------------------
Changed:
 - TokenClient Bug Fix: Check encoded token type before attempting to decode.


[0.0.19] - 2021-01-26
---------------------
Changed:
 - TokenClient Code Change: Separate token encode and decode steps (for
   debugging).


[0.0.18] - 2020-07-30
---------------------
Changed:
 - TokenClient Bug Fix: Correctly update request header after refreshing token.


[0.0.17] - 2020-07-29
---------------------
Changed:
 - TokenClient: update request header after refreshing token, and before
   retrying request.


[0.0.16] - 2020-07-29
---------------------
Changed:
 - TokenClient: When creating token, convert from bytes to a string.


[0.0.15] - 2020-07-29
---------------------
Changed:
 - TranslatorClient.work_exists: Report all content from the translation API
   response when this method fails (investigation).


[0.0.14] - 2020-07-29
---------------------
Changed:
 - TranslatorClient.work_exists: Report 'data' content from the translation API
   when this method fails (investigation).


[0.0.13] - 2020-07-27
---------------------
Added
 - Token creation option: The TokenClient can now also be used to create a
   token, based on the logic used by the Tokens API.


[0.0.12] - 2020-05-20
---------------------
Added
 - New client: AltmetricsClient


[0.0.11] - 2020-04-15
---------------------
Changed
 - TranslatorClient.prepare_uri: Now returns the URI as a string in the format
   expected by TranslatorClient.get_work_uris. 


[0.0.10] - 2020-04-15
---------------------
Changed
 - TranslatorClient.post_new_work: 'uris' parameter now assumes the same format
   as the output from TranslatorClient.get_work_uris, which was causing errors.
   (bug fix)


[0.0.9] - 2020-04-06
---------------------
Added
 - translator: Reference variables for work types and URI schemes.
 - translator: Check if a work exists.
 - translator: Fetch all URIs associated with a work.
 - translator: Post new work.


[0.0.8] - 2020-03-06
---------------------
Changed
 - Make requirements for flexible to avoid conflicts with other packages.


[0.0.7] - 2020-03-06
---------------------

Added
 - Release notes

Changed
 - Strip trailing slash from TranslatorClient API base.
