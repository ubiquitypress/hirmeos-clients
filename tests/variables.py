obscure_valid_doi = 'info:doi:10.5334/bbc:1'  # I think this is valid

format_test_uri_values = [
    'https://www.ubiquitypress.com/site/books/10.5334/BBC/',
    'info:doi:10.5334/BBC',
    'URN:ISBN:9781912482245',
    'URN:UUID:A7FF33CD-56A7-4594-8ACA-12781296C0A1',  # for some reason
]

canonical_uri_values = [
    'https://www.ubiquitypress.com/site/books/10.5334/bbc',
    'info:doi:10.5334/bbc',
]

expected_uri_values = [
    'https://www.ubiquitypress.com/site/books/10.5334/bbc',
    'info:doi:10.5334/bbc',
    'urn:isbn:9781912482245',
    'urn:uuid:a7ff33cd-56a7-4594-8aca-12781296c0a1',
]

expected_uri_schemes = [
    'https',
    'info:doi',
    'urn:isbn',
    'urn:uuid',
]

expected_prepared_uris = [
    {'URI': 'https://www.ubiquitypress.com/site/books/10.5334/bbc'},
    {'URI': 'info:doi:10.5334/bbc'},
    {'URI': 'urn:isbn:9781912482245'},
    {'URI': 'urn:uuid:a7ff33cd-56a7-4594-8aca-12781296c0a1'},
]


expected_prepared_canonical_uris = [
    {'URI': 'urn:isbn:9781912482245'},
    {'URI': 'urn:uuid:a7ff33cd-56a7-4594-8aca-12781296c0a1'},
    {'URI': 'https://www.ubiquitypress.com/site/books/10.5334/bbc', 'canonical': True},
    {'URI': 'info:doi:10.5334/bbc', 'canonical': True}
]

expected_decoded_token = {
    'authority': 'admin',
    'email': 'test.token@testemail.test',
    'name': 'test admin',
    'sub': 'acct:test.token@testemail.test'
}
